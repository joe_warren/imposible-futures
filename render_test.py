#! /usr/bin/env python
from pyassimp import pyassimp
import sys

import json

import pygame
from pygame.locals import *

import math
import numpy

import OpenGL.GL as GL

import matrix

try: 
    from PIL.Image import open as ImOpen
except ImportError, err:
    from Image import open as ImOpen

SCREEN_SIZE = (800, 600)

def eventLoop():
    for event in pygame.event.get():
        if event.type == QUIT:
           return False
        if event.type == KEYUP and event.key == K_ESCAPE:
           return False           
    return True

def printShaderLog(vert, frag):
    print( "== VERTEX SHADER ==" )
    print( GL.glGetShaderInfoLog(vert) )
    print( "== FRAGMENT SHADER ==" )
    print( GL.glGetShaderInfoLog(frag) )

def printProgramLog(p):
    print( GL.glGetProgramInfoLog(p) )

def loadShader(vertex, fragment):
    vtext = open(vertex, 'r').readlines()
    ftext = open(fragment, 'r').readlines()

    v = GL.glCreateShader(GL.GL_VERTEX_SHADER)
    f = GL.glCreateShader(GL.GL_FRAGMENT_SHADER)

    GL.glShaderSource(v, vtext)
    GL.glShaderSource(f, ftext)
  
    GL.glCompileShader(v)
    GL.glCompileShader(f)
    
    printShaderLog(v, f)

    p = GL.glCreateProgram()
    GL.glAttachShader(p,v)
    GL.glAttachShader(p,f)

    GL.glLinkProgram(p)

    printProgramLog(p)

    GL.glUseProgram(p)

    return p

def loadBuffer(rawdata):
    b = GL.glGenBuffers(1)
    GL.glBindBuffer(GL.GL_ARRAY_BUFFER, b)
    GL.glBufferData(GL.GL_ARRAY_BUFFER, numpy.array(rawdata, numpy.float32), GL.GL_STATIC_DRAW)
    return b;

def projectionMatrix( fov, aspect, nearDist, farDist, leftHanded=True ):

    frustumDepth = farDist - nearDist
    oneOverDepth = 1.0 / frustumDepth

    uh =  1.0 / math.tan(0.5 * fov)

    mult = 1.0
    if( leftHanded == False ):
        mult = -1.0

    p = numpy.array([uh/aspect, 0, 0, 0,
                     0, uh*mult, 0, 0,
                     0, 0, -farDist * oneOverDepth, -farDist*nearDist*oneOverDepth,
                     0, 0, -1, 0],
          numpy.float32)
    return p

def modelViewMatrix():

    mv = numpy.array([1,0, 0, 0,
                     0, 0, 1, 1,
                     0, -1, 0, -10,
                     0, 0, 0, 1], numpy.float32)
    return mv


def aspectRatio():
    return float(SCREEN_SIZE[0])/float(SCREEN_SIZE[1])

def buildElementArray( faces ):
    indices = [i for f in faces for i in f.indices]
    return indices

def loadElementArray( indices ):
    b = GL.glGenBuffers(1)
    GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, b)
    GL.glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, numpy.array(indices, numpy.uint32), GL.GL_STATIC_DRAW)
    return b

def loadTexture( filename ):
    im = ImOpen(filename)
    try: 
       ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBA", 0, -1)
    except SystemError:
       ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)
    ID = GL.glGenTextures(1) 

    GL.glBindTexture(GL.GL_TEXTURE_2D, ID)
    GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT,1)

    GL.glTexImage2D( GL.GL_TEXTURE_2D, 0, GL.GL_RGBA, 
      ix, iy, 0, GL.GL_RGBA, GL.GL_UNSIGNED_BYTE, image ) 

    GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MAG_FILTER,GL.GL_LINEAR);
    GL.glTexParameteri(GL.GL_TEXTURE_2D,GL.GL_TEXTURE_MIN_FILTER,GL.GL_NEAREST);

    return ID

def fixTexCoord( coord ):
    if coord == None:
       return[0.0,0.0,0.0];
    return coord

def getFourBoneValues(vals):
    return [ vals[i] if i < len(vals) else 0 for i in range(0,4) ]

def getBoneIdAndWeights(bones, length):
    Ids = [[] for x in range(0,length)]
    Weights = [[] for x in range(0,length)]
    for i, b in enumerate(bones):
        for w in b.weights:
            Ids[w.mVertexId].append(i)
            Weights[w.mVertexId].append(w.mWeight)
    return  ([getFourBoneValues(i) for i in Ids],
             [getFourBoneValues(i) for i in Weights]) 

def buildBonesMatrices( length ):
    return[ matrix.Identity() for i in range(0, length) ]

def unrollBonesMatrices( matrices ):
    return [v for m in matrices for v in m.unroll() ] 

def parseBonesStructure( bones ):
    children = dict();
    for b in bones:
        children[b["name"]] = b["children"]

    allChildren = [ a for l in children.values() for a in l]
    rootList = map( lambda x: x["name"], bones )
    rootList = filter( lambda x: not x in allChildren, rootList )
    assert(len(rootList) == 1)
    return rootList[0], children

def printBonesStructure(root, children, indent = 0):
    print("%s%s" %( " "*indent, root) )
    for c in children[root]:
        printBonesStructure(c, children, indent+1)

def main():
    print sys.argv[1]

    aiProcess_Triangulate = 0x8
    scene = pyassimp.load(sys.argv[1], aiProcess_Triangulate)

    print( str(len(scene.meshes)) + " meshes")
    assert len(scene.meshes)
    mesh = scene.meshes[0]

    print( str(len(mesh.bones)) + " bones")
    print( str(len(mesh.vertices)) + " vertices")
    print( str(len(mesh.faces)) + " faces")
    assert len(mesh.vertices)
    assert len(mesh.bones)

    pygame.init()
    screen = pygame.display.set_mode(SCREEN_SIZE, HWSURFACE|OPENGL|DOUBLEBUF)
    GL.glViewport(0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1])
    GL.glEnable(GL.GL_DEPTH_TEST)
    #GL.glEnable(GL.GL_VERTEX_ARRAY)
    GL.glEnable(GL.GL_BLEND)
    GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)

    print( (mesh.bones[0].weights[0].mWeight) )
    GL.glDisable(GL.GL_CULL_FACE)

    with open(sys.argv[2]) as jfile:
        animdata = json.load(jfile)
        bonesRoot, bonesChildren = parseBonesStructure(animdata["bones"])
        printBonesStructure( bonesRoot, bonesChildren )
    texture = loadTexture( sys.argv[3] )

    program = loadShader( "xyz.vert", "xyz.frag")

    vertices = loadBuffer( [a for v in mesh.vertices for a in v] )
    normals = loadBuffer( [a for v in mesh.normals for a in v] )
    unrefinedTexcoords =  [a for v in mesh.texcoords for a in v] 
    unrefinedTexcoords = [fixTexCoord(t) for t in unrefinedTexcoords]
    rawBoneIds, rawBoneWeights = getBoneIdAndWeights(mesh.bones, len(mesh.vertices))
    boneIds = loadBuffer([ i for i in rawBoneIds])
    boneWeights = loadBuffer([i for i in rawBoneWeights])

    bonesMatrices = buildBonesMatrices(len(mesh.bones))

    texcoords = loadBuffer( [a for v in unrefinedTexcoords for a in v] )

    indices = buildElementArray(mesh.faces)
    elements = loadElementArray(indices)

    positionAttrib = GL.glGetAttribLocation( program, "aVertexPosition" )
    normalAttrib = GL.glGetAttribLocation( program, "aVertexNormal" )
    texcoordAttrib = GL.glGetAttribLocation( program, "aVertexTexCoord" )
    boneIdsAttrib = GL.glGetAttribLocation( program, "aBoneIds" )
    boneWeightsAttrib = GL.glGetAttribLocation( program, "aBoneWeights" )
    pMatrixUniform = GL.glGetUniformLocation( program, "uPMatrix" )
    mvMatrixUniform = GL.glGetUniformLocation( program, "uMVMatrix" )
    bonesMatricesUniform = GL.glGetUniformLocation( program, "uBonesMatrices" )
    textureUniform = GL.glGetUniformLocation( program, "uSamplerTexture" )

    pMatrix = matrix.ProjectionMatrix( 45, aspectRatio(), 0.01, 100.0 )
 
    mvMatrix = matrix.Identity()
    mvMatrix.rotate([1.0,0.0,0.0],90)

    GL.glClearColor(1.0, 1.0, 1.0, 0.0)

    GL.glEnable(GL.GL_TEXTURE_2D)

    GL.glTexEnvf(GL.GL_TEXTURE_ENV, GL.GL_TEXTURE_ENV_MODE, GL.GL_DECAL)
 
    i = 0
    while eventLoop():
        i += 1

        mvMatrix.push()
        mvMatrix.rotate([0.0,1.0,0.0],i*0.5)
        mvMatrix.translate([0.0,0.0,-15.0])

        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        GL.glUseProgram(program)

        GL.glUniformMatrix4fv(pMatrixUniform, 1, True, pMatrix.unroll())
        GL.glUniformMatrix4fv(mvMatrixUniform, 1, True, mvMatrix.unroll())
        GL.glUniformMatrix4fv(bonesMatricesUniform, len(mesh.bones), True, unrollBonesMatrices(bonesMatrices) )

        mvMatrix.pop()

        GL.glActiveTexture( GL.GL_TEXTURE0)
        GL.glBindTexture(GL.GL_TEXTURE_2D, texture)
        GL.glUniform1i(textureUniform, 0)

        GL.glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, elements)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vertices)
        GL.glEnableVertexAttribArray( positionAttrib )
        GL.glVertexAttribPointer(positionAttrib, 3, GL.GL_FLOAT, False, 0, None)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, vertices)
        GL.glEnableVertexAttribArray( positionAttrib )
        GL.glVertexAttribPointer(positionAttrib, 3, GL.GL_FLOAT, False, 0, None)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, normals)
        GL.glEnableVertexAttribArray( normalAttrib )
        GL.glVertexAttribPointer(normalAttrib, 3, GL.GL_FLOAT, False, 0, None)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, texcoords)
        GL.glEnableVertexAttribArray( texcoordAttrib )
        GL.glVertexAttribPointer(texcoordAttrib, 3, GL.GL_FLOAT, False, 0, None)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, boneIds)
        GL.glEnableVertexAttribArray( boneIdsAttrib )
        GL.glVertexAttribPointer(boneIdsAttrib, 4, GL.GL_FLOAT, False, 0, None)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, boneWeights)
        GL.glEnableVertexAttribArray( boneWeightsAttrib )
        GL.glVertexAttribPointer(boneWeightsAttrib, 4, GL.GL_FLOAT, False, 0, None)

        GL.glDrawElements(GL.GL_TRIANGLES, len(indices),GL.GL_UNSIGNED_INT, None)

        pygame.display.flip()

    pyassimp.release(scene)

    return 0

if __name__ == "__main__":
    sys.exit(main())
