import math
import numpy as np


class Matrix:
   def __init__(self, m):
       self.mat = np.matrix(m, np.float32)
       self.stack = []
   def unroll(self):
       return np.array([self.mat[i,j] for i in range(0,4) for j in range(0,4)],
           np.float32 )
   def push(self):
       self.stack.append(self.mat)
   def pop(self):
       self.mat = self.stack.pop()
   def translate( self, v ):
       for i in range(0,3):
           self.mat[i,3] += v[i]
   def rotate( self, axis, angle ):
       u = axis
       c = math.cos(math.radians(angle))
       s = math.sin(math.radians(angle))
      
       x = 0
       y = 1
       z = 2

       r = np.matrix([
          [(c+u[x]*u[x]*(1.0-c)),
           (u[x]*u[y]*(1.0-c)-u[z]*s), 
           (u[x]*u[z]*(1.0-c)+u[y]*s),
           0.0
          ],
          [(u[x]*u[y]*(1.0-c)+u[z]*s),
           (c+u[y]*u[y]*(1.0-c)),
           (u[y]*u[z]*(1.0-c)-u[x]*s),
           0.0
          ],
          [(u[x]*u[z]*(1.0-c)-u[y]*s),
           (u[y]*u[z]*(1.0-c)+u[x]*s),
           (c+u[z]*u[z]*(1-c)),
           0.0
          ],
          [0.0,0.0,0.0,1.0]], np.float32)
       self.mat = r * self.mat

def Identity():
    return Matrix([[1.0,0.0,0.0,0.0],
                   [0.0,1.0,0.0,0.0],
                   [0.0,0.0,1.0,0.0],
                   [0.0,0.0,0.0,1.0]])

def ProjectionMatrix( fov, aspect, nearDist, farDist, leftHanded=True ):
    frustumDepth = farDist - nearDist
    oneOverDepth = 1.0 / frustumDepth

    uh =  1.0 / math.tan(0.5 * math.radians(fov))

    mult = 1.0
    if( leftHanded == False ):
        mult = -1.0

    p = [[uh/aspect, 0, 0, 0],
         [0, uh*mult, 0, 0],
         [0, 0, -farDist * oneOverDepth, -farDist*nearDist*oneOverDepth],
         [0, 0, -1, 0]]
    return Matrix(p)
