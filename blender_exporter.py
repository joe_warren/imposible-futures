import bpy
import json

scene = bpy.context.scene

obj_active = scene.objects.active

armature = obj_active.find_armature()

bones = []

for b in armature.pose.bones:
    bone = {"name": b.name, "x":b.location.x, "y":b.location.y, "z":b.location.z}
    bone["children"] = [];
    for c in b.children:
        bone["children"].append(c.name)
    bones.append(bone)

curves = [];

action = bpy.data.actions['ArmatureAction']
for fcu in action.fcurves:
    curve = { "start" : fcu.range().x, "end": fcu.range().y, "points" : [] }
    curve["bone"] = fcu.data_path.split('"')[1]
    curve["datatype"] = fcu.data_path.split('.')[-1]
    curve["channel"]= fcu.array_index
    print(curve["points"])
    for points in fcu.keyframe_points:
        curve["points"].append( { "time":points.co.x, "value": points.co.y} ) 
    curves.append(curve)
    
with open("file.json", "w") as f:
    json.dump({"bones": bones, "curves": curves}, f, indent=1)
