attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec3 aVertexTexCoord;
attribute vec4 aBoneIds;
attribute vec4 aBoneWeights;

varying vec3 texcoord;
varying vec3 normal;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uBonesMatrices[25];

void main(void) {
  mat4 boneMatrix = mat4(0);
  for(int i = 0; i<4; i++){ 
      boneMatrix += aBoneWeights[i] * uBonesMatrices[int(aBoneIds)];
  }
  gl_Position = uPMatrix * uMVMatrix * boneMatrix * vec4(aVertexPosition, 1.0);
  normal = normalize(vec3( uMVMatrix* vec4(aVertexNormal,0.0) ));
  texcoord = aVertexTexCoord;
}

