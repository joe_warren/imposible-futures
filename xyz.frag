varying vec3 normal;
varying vec3 texcoord;

uniform sampler2D uSamplerTexture;

float cellShadeIntensity( float i ){
   return ceil(i*2.0)/2.0;
}

void main(void) {
  float i = max(dot(normalize(vec3(1.0,1.0,1.0)),normalize(normal)),0.0);
  i = 0.1 + 0.9*cellShadeIntensity(i);
  vec4 tex = texture2D( uSamplerTexture, texcoord.xy );
  vec3 colour = tex.rgb * tex.a + vec3( 0.0,0.0,1.0) * (1.0-tex.a);
  gl_FragColor = vec4(colour*i,1.0);
}
